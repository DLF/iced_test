use iced::button;
use iced::{
    executor, window, Application, Button, Color, Column, Command, Container, Element, Settings,
    Text,
};

#[derive(Debug, Clone, Copy)]
pub enum Message {
    IncrementPressed,
    DecrementPressed,
}

struct App {
    value: i32,
    increment_button: button::State,
    decrement_button: button::State,
}

impl Application for App {
    type Executor = executor::Default;
    type Message = Message;
    type Flags = ();

    fn new(_flags: ()) -> (App, Command<Self::Message>) {
        (
            App {
                value: 10,
                increment_button: button::State::new(),
                decrement_button: button::State::new(),
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        String::from("Test the Ice")
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            Message::IncrementPressed => {
                self.value += 1;
            }
            Message::DecrementPressed => {
                self.value -= 1;
            }
        }
        Command::none()
    }

    fn view(&mut self) -> Element<Self::Message> {
        let content = Column::new()
            .push(
                Button::new(&mut self.increment_button, Text::new("+"))
                    .on_press(Message::IncrementPressed),
            )
            .push(Text::new(self.value.to_string()).size(50))
            .push(
                Button::new(&mut self.decrement_button, Text::new("-"))
                    .on_press(Message::DecrementPressed),
            );
        Container::new(content).into()
    }

    fn background_color(&self) -> Color {
        //Color::TRANSPARENT
        Color {
            r: 0.0,
            g: 0.5,
            b: 0.0,
            a: 0.5,
        }
    }
}

fn main() {
    let mut win_settings = window::Settings::default();
    win_settings.transparent = true;
    let mut settings = Settings::default();
    settings.window = win_settings;
    App::run(settings).expect("app crash");
}
